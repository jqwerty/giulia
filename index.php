<!DOCTYPE html>
    <head>
        <title>Giulia Search Engine</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="jqwerty">
        <link rel="stylesheet" href="main.css" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="shortcut icon" type="image/png" href="icon.png">
    </head>
    <body>
        <div class="wrapper">
            <div class="search-box">
                <img src="logo.png" class="logo">
                <center><div class="form-wrap">
                    <form action="search" method="get" name="q">
                        <input class="search-text" type="text" name="key"><div class="times-button result-none"><i class="fas fa-times"></i></div><button type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div></center>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
        <script>
            $(document).ready(function(){
                $('.search-text').keyup(function() {
                    if($(this).val() == '') {
                        $('.times-button').addClass('result-none');
                    }else {
                        $('.times-button').removeClass('result-none');
                    }
                });

                $('.times-button').click(function() {
                    $('.search-text').val('');
                    $('.times-button').addClass('result-none');
                });
            });
        </script>

    </body>
</html>