<?php
$key = $_GET['key'];
?>
<!DOCTYPE html>
  <head>
      <title><?php echo "$key | Giulia Search Engine Beta"; ?>
      </title>
      <meta charset="UTF-8">
      <link rel="stylesheet" href="main.css" type="text/css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
      <link rel="shortcut icon" type="image/png" href="icon.png">
    </head>
    <body>
      <div class="wrapper">
        <header class="header">
          <div class="logo-mini"><img src="icon.png" class="icon-left"></div>
          <div class="search-box-mini">
          <form action="search" method="get" name="q">
            <input class="search-text" type="text" value ="<?php echo $key; ?>" name="key"><div class="times-button result-none"><i class="fas fa-times"></i></div><button type="submit"><i class="fas fa-search"></i></button>
          </form>
          </div>
        </header>