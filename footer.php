  <footer class="footer">
    <div class="footer-copyright">
    &copy; <script>
        var date = new Date();
        document.write(date.getFullYear());
    </script> Giulia Search Engine
    </div>
  </footer>
  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
        <script>
            $(document).ready(function(){
                $('.search-text').keyup(function() {
                    if($(this).val() == '') {
                        $('.times-button').addClass('result-none');
                    }else {
                        $('.times-button').removeClass('result-none');
                    }
                });

                $('.times-button').click(function() {
                    $('.search-text').val('');
                    $('.times-button').addClass('result-none');
                });
            });
        </script>
</body>
</html>
