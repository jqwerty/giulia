<?php


include "header.php";

include "lib.php";

$key = $_GET['key'];

$key1 = str_replace(" ","+",$key);

$keyword = strtolower($key1);

//ADDRESS AHMIA
$address = "https://ahmia.fi/search/?q=";
$url = $address . $keyword;

//CREATE DOM DOCUMENT for AHMIA

$read = curlAhmia("$url");

$dom = new DOMDocument();

@$dom->loadHTML($read);

//SEARCH CLASS
$classname = "searchResult";
$finder = new DomXPath($dom);
$spaner = $finder->query("//*[contains(@class, '$classname')]");

//GRAB DATA FROM CLASS
$span = $spaner->item(0);

$title = $span->getElementsByTagName('h4');
$link = $span->getElementsByTagName('cite');
$desc = $span->getElementsByTagName('p');

$no = 0;

$number = $no++;

$data = array();
foreach ($title as $val){
  $data[] = array(
    'title' => $title->item($number)->nodeValue,
    'link' => $link->item($number)->nodeValue,
    'desc' => $desc->item($number)->nodeValue,
  );
  $number++;
}


$total = count($data);
?>
<div class="body-wrap">
<div class="showing">
Showing result for <b><?php echo $key; ?></b>, <?php echo $total . " Result found"; ?>
</div>


<div class="search-result">
<ul>

<?php
//PAGINATION
$record = 10;
$page = ! empty( $_GET['page'] ) ? (int) $_GET['page'] : 1;
$paging = ceil($total / $record);

$page = max($page, 1);
$page = min($page, $paging);
$offset = ($page - 1) * $record;
if( $offset < 0 ) $offset = 0;

$data = array_slice( $data, $offset, $record );

foreach($data as $val)
{
?>

    <li>
      <a href="//<?php echo $val['link']; ?>"><b><?php echo $val['title']; ?></b></a>
      <div class="search-result-desc"><?php echo $val['desc']; ?></div>
      <div class="search-result-url">
      <?php
      if(strlen($val['link']) > 30) {
        echo substr($val['link'], 1, 30) . '...';
      }else {
        echo $val['link'];
      }
      ?>
      </div>
    </li>

<?php
}

?>
</ul>
</div>
</div>
<?php

$url_link = 'search?key=' . $keyword . '&q=Search&page=%d';

$pagerContainer = '<center><div class="paginate" style="width: 300px;">';
if( $paging != 0 )
{
  if( $page == 1 )
  {
    $pagerContainer .= '';
  }
  else
  {
    $pagerContainer .= sprintf( '<a href="' . $url_link . '">PREV</a> ', $page - 1 );
  }
  $pagerContainer .= ' <span> page <strong>' . $page . '</strong> from ' . $paging . '</span>';
  if( $page == $paging )
  {
    $pagerContainer .= '';
  }
  else
  {
    $pagerContainer .= sprintf( ' <a href="' . $url_link . '">NEXT</a>', $page + 1 );
  }
}
$pagerContainer .= '</div></center>';

echo $pagerContainer;

include "footer.php";
?>
